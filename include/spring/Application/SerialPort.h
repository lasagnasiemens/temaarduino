#pragma once
#define ARDUINO_WAIT_TIME 2000
#define MAX_DATA_LENGTH 255
#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>
#include <windows.h>
namespace Spring
{
	class SerialPort
	{
	private:
		HANDLE handler;
		bool connected;
		COMSTAT status;
		DWORD errors;
	public:
		SerialPort(char *portName);
		~SerialPort();

		int readSerialPort(char *buffer, unsigned int buf_size);
		bool writeSerialPort(char *buffer, unsigned int buf_size);
		bool isConnected();
	};

}
